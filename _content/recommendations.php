<?php

function rec($code){
    switch($code){
        case 'north':
            return 'I worked with Adam at the North Institute when he was a student intern for us. He helped us re-designed our web page, and his design makes the site look much better. To this day we are still using the structure that he helped us to reorganize. I find him to have a very sound mental structure of how things should work, and a good sense of visual design. I enjoyed working with him and I find him respectful, helpful and productive.';
            break;
        case 'tands-tim':
            return 'Adam is an excellent employee. He\'s always striving to learn new things. He also frequently makes suggestions for improvements. He really goes above and beyond.';
            break;
        case 'tands-chris':
            return 'Adam is a very self-motivated person and a hard worker. He always looks for new and better ways of doing things that help to improve production and make things run smoother for everybody involved in a project, and even the company itself. His passion for web design is very apparent and he would be an asset to any company he works for.';
            break;
        case 'brock':
            return 'Adam is a very intelligent person. He has always been the go to guy for computer issues within my group of friends because of his technological savvy. He is also a very reliable person. Having been an officer in Psi Epsilon with Adam for 3 years, I can say with confidence that he will not hesitate to be the one that does what needs to be done.';
            break;
        case 'hicks':
            return 'Throughout Adam\'s academic career, he has been involved in all levels of leadership in several organizations. I have managed him directly in the Student Government Association and he has been senior to me as an Executive officer in Psi Epsilon. In all of his positions Adam has shown the qualities of a true leader through his work ethic, creative thinking, strong organizational skills, and an uncanny ability to just get things done.';
            break;            
    }
}
?>

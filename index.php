<?php
require_once('_content/recommendations.php');
?>
<html>
    <head>
        <title>Adam Booth Resume</title>

        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="_css/reset.css" />
        <link rel="stylesheet" type="text/css" href="_css/style.css" />
        <link rel="stylesheet" type="text/css" media="print" href="_css/print.css" />
        <link href='http://fonts.googleapis.com/css?family=Anton' rel='stylesheet' type='text/css'/>

        <link rel="icon" type="image/png" href="_img/favicon.ico"/>

        <script type="text/javascript" src="_js/jquery.js"></script>          
        <script type="text/javascript">
            $(document).ready(function() {
                $('a#tandsRefLink').click(function(){
                    
                    $('.linked').css('color','black');
                    window.setTimeout(function() {
                        $('.tandsBookmark').css('color','#268CFF');
                    }, 500);
                    
                });
                
                $('a#northRefLink').click(function(){
                    $('.linked').css('color','black');
                    window.setTimeout(function() {
                        $('.northBookmark').css('color','#268CFF'); 
                    }, 500);
                    
                });
                
                $('a#sgaRefLink').click(function(){
                    $('.linked').css('color','black');
                    
                    window.setTimeout(function() {
                        $('.sgaBookmark').css('color','#268CFF');
                    }, 500);
                   
                });
                
                
                $('a#psiRefLink').click(function(){
                    $('.linked').css('color','black');
                    window.setTimeout(function() {
                        $('.psiBookmark').css('color','#268CFF'); 
                    }, 500);
                   
                });
               
                
                
                $('#expander').click(function(){
                    $('p.inDepth, #expander span#expand, #expander span#collapse').toggle('fast'); 
                });
            });
            
        </script> 

        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-26768692-1']);
            _gaq.push(['_setDomainName', 'ambooth.com']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
    </head>
    <body>
        <div class="noPrint" id="backButton">
            <a class="nopic" href="/">Back to site</a>
        </div>


        <div id="paper">
            <div class="noPrint" id="expander">
                <span id="expand">Show me the specifics only</span>
                <span id="collapse">Show me everything</span>
            </div>

            <div id="addressBox">
                700 NE 122nd Street<br/>
                Oklahoma City, OK 73114<br/>

                <a class="nopic" href="mailto:adam@ambooth.com">adam@ambooth.com</a><br/>
                785-813-1748
            </div>
            <h1>Adam Booth</h1>
            <h2>Web Developer</h2>
            <div class="profile">
                <a class="direct" name="profile"><h3>Profile</h3></a>
                <p><?php include('_content/profile.txt'); ?></p>
                <p>

                </p>
            </div>

            <div class="entry">
                <h3>Technical Application</h3>
                <p>
                    Over <?= date('Y') - 2007 ?> years of Information Systems experience.
                    Excellent technical support skills with a passion for web development.
                    Able to quickly and effortlessly troubleshoot and analyze both software and hardware issues.
                    Rapidly learns applications and optimizes via work flows efficiently.
                    Extensive experience with Adobe Photoshop, Illustrator and Fireworks.
                </p>
                <div class="entrySectionCenter">
                    <div style="float:left">
                        <div class="entryName">Development Tools</div>
                        <ul>
                            <li>Netbeans IDE</li>
                            <li>Eclipse IDE</li>
                            <li>Visual Studio 2010</li>
                            <li>Git / Github</li>
                        </ul>
                    </div>
                    <div style="float:left; margin-left: 20px">
                        <div class="entryName">Languages & Frameworks</div>
                        <ul>
                            <li>PHP</li>
                            <li>HTML & CSS</li>
                            <li>jQuery & Javascript</li>
                            <li>Regular Expressions (RegEx)</li>
                            <li>C++</li>
                            <li>C#</li>
                            <li>MySQL & SQL</li>
                            <li><a target="_blank" href="http://fuelphp.com/">Fuel PHP MVC Framework</a></li>
                            <li>Wordpress: Themes & Plugins</li>
                            <li><a target="_blank" href="http://twitter.github.com/bootstrap/">Bootstrap Framework</a></li>
                        </ul>
                    </div>
                    <div style="float:left; margin-left: 20px">
                        <div class="entryName">Other Application Experience</div>
                        <ul>
                            <li>Adobe Photoshop</li>
                            <li>Adobe Fireworks</li>
                            <li>Adobe Illustrator</li>
                            <li>Microsoft Office Products</li>
                            <li>Apple Final Cut Pro</li>
                            <li>MAMP</li>
                            <li>MySQL Workbench</li>
                        </ul>
                    </div>
                    <div class="clearer"></div>
                </div>
            </div>

            <div class="entry">
                <a class="direct" name="education"><h3>Education</h3></a>
                <div class="years">2007 - 2012</div>
                <div class="entrySection">
                    <div class="entryName">Oklahoma Christian University</div>
                    <div>Edmond, Oklahoma</div>
                    <div>Information Systems Management</div>
                    <div>Graduated April 2012</div>
                </div>
            </div>
            <a class="direct" name="xp"><h3>Experience</h3></a>

            <div class="entry">
                <div class="years">December 2012 - Present</div>
                <div class="entrySection">
                    <div class="entryName">Web Developer, Flogistix, LP</div>
                    <div class="entrySubText">Oklahoma City, OK</div>
                    <p class="inDepth">
                    </p>
                   <!-- <ul class="inDepth">
                        <li></li>
                    </ul> -->
                </div>
            </div>

            <div class="entry">
                <div class="years">February 2012 - November 2012</div>
                <div class="entrySection">
                    <div class="entryName">Internal Developer, LifeChurch.tv</div>
                    <div class="entrySubText">Edmond, OK</div>
                    <p class="inDepth">
                        Web developer position focusing on internal projects used by staff and volunteers.
                        Utilize PHP frameworks such as FuelPHP and Git/Github for versioning and collaboration.
                        Work with numerous REST-based APIs including Google Maps, Fellowship One, CellForce SMS systems, and Active Directory interactions.
                        Create and maintain Wordpress websites and plugins used by team members.
                    </p>
                    <ul class="inDepth">
                        <li>Heavy use of FuelPHP Framework and Git/Github</li>
                        <li>Heavy use of APIs for Google Maps, Fellowship One, CellForce SMS systems, and Active Directory interactions</li>
                        <li>Create web applications used by staff and volunteers</li>
                        <li>Create and maintain Wordpress websites and plugins</li>
                    </ul>
                </div>
            </div>
            <div class="entry">
                <div class="years">April 2010 - September 2011</div>
                <div class="entrySection">
                    <div class="entryName">Programmer, T&S Web Design</div>
                    <div class="entrySubText">Edmond, OK</div>
                    <p class="inDepth">
                        Lead developer and go-to guy for technical support concerns and issues.
                        Gracefully transitioned company into using Google Apps for email, calendars, and their internal wiki system.
                    </p>
                    <p>
                        <span class="noPrint">(<a class="nopic" id="tandsRefLink" href="#tands"><strong>Jump to Recommendations</strong></a>)</span>
                    </p>
                    <ul class="inDepth">
                        <li>Create websites using WordPress framework, HTML, PHP, CSS, and JQuery based on designs made in-house</li>
                        <li>Conduct training meetings for clients</li>
                        <li>Technical support for staff and clients</li>
                        <li>On site tech support for clients and even training sessions if requested</li>
                    </ul>
                </div>
            </div>

            <div class="entry">
                <div class="years">April 2010 - May 2010</div>
                <div class="entrySection">
                    <div class="entryName">Programmer, North Institute, Oklahoma Christian University</div>
                    <div class="entrySubText">Edmond, OK</div>
                    <p class="inDepth">Student intern position. Made modifications to organization website making it easier to
                        navigate and find content. Further develop internal media sharing project used to distribute content to students. 
                    </p><p><span class="noPrint">(<a id="northRefLink" class="nopic" href="#northBookmark"><strong>Jump to Recommendation</strong></a>)</span></p>
                    <ul>
                        <li>Redesign and manage North Institute website - <a href="http://ni.oc.edu">ni.oc.edu</a></li>
                        <li>Work on internal media sharing software used to distribute videos and other media to students using C#, ASP, and HTML</li>
                    </ul>
                </div>
            </div>

            <div class="entry">
                <div class="years">August 2009 - April 2010</div>
                <div class="entrySection">
                    <div class="entryName">Software Technician, Support Central, Oklahoma Christian University</div>
                    <div class="entrySubText">Edmond, OK</div>
                    <p class="inDepth">
                        Software Support Technician at Oklahoma Christian University.
                        Provide friendly support to students and teachers.
                        Adapted quickly to workflow, mentored new and current student workers.
                    </p>
                    <ul class="inDepth">
                        <li>Provide in-person technical support to fellow students and teachers</li>
                        <li>Provide support for teachers in classroom (On and off the clock)</li>
                        <li>Minor hardware diagnostics before passing computer to hardware department if necessary</li>
                    </ul>
                </div>
            </div>

            <div class="entry">
                <div class="years">May 2009 - August 2009</div>
                <div class="entrySection">
                    <div class="entryName">Product Specialist, iCafe</div>
                    <div class="entrySubText">Lawrence, KS</div>
                    <p class="inDepth">
                        Technical support specialist providing support for clients.
                        Diagnose and repair both hardware and software issues on Macintosh computers.
                    </p>
                    <ul>
                        <li>Provide in-person and phone support to clients</li>
                        <li>Diagnose and repair Apple computers with hardware or software issues</li>
                    </ul>
                </div>
            </div>

            <div class="entry">
                <div class="years">January 2008 - April 2008</div>
                <div class="entrySection">
                    <div class="entryName">Martial Arts Instructor, Premier Marital Arts</div>
                    <div class="entrySubText">Edmond, OK</div>
                    <p class="inDepth">
                        Second degree black belt in Tae Kwon Do and Mixed Martial Arts, was hired to teach
                        kids classes. Create T-Shirt designs and images used for website.
                    </p>
                    <ul>
                        <li>Mentor students of various belt levels</li>
                        <li>Taught multiple kids classes as lead instructor</li>
                        <li>Design images used on website and T-Shirt designs</li>
                    </ul>
                </div>
            </div>
            <!--            <div class="spacer"></div>-->
            <!--        Other experience-->
            <div class="entry">
                <a class="direct" name="otherXP"><h3>Other Experience</h3></a>
                <div class="years">September 2008 - 2012</div>
                <div class="entrySection">
                    <div class="entryName">Executive Secretary/Webmaster, Psi Epsilon Social Service Club</div>
                    <div class="entrySubText">Oklahoma Christian University, Edmond, OK</div>
                    <p class="inDepth">
                        Began social service club named Psi Epsilon.
                        Today Psi has more that 70 members and is one of the largest mens clubs on Oklahoma Christian's campus.
                        Served as an executive officer for 4 years and created fully functional <a target="_blank" href="http://www.psiep.com/">website</a>
                        that is used regularly by members and potential members. Maintain emailing lists and overall organization of club.</p>
                    <p><span class="noPrint">(<a id="psiRefLink" class="noPrint nopic" href="#psiBookmark"><strong>Jump to Recommendation</strong></a>)</span></p>
                    <ul>
                        <li>Founding Member - Started club and recruited 20 other members at beginning</li>
                        <li>Create and maintain club website - <a target="_blank" href="http://www.psiep.com/">psiep.com</a></li>
                        <li>In charge of taking meeting notes, maintain emailing lists</li>
                    </ul>
                </div>
            </div>

            <div class="entry">
                <div class="years">September 2007 - April 2009</div>
                <div class="entrySection">
                    <div class="entryName">Male Representative, OC Student Government Association</div>
                    <div class="entrySubText">Oklahoma Christian University, Edmond, OK</div>
                    <p class="inDepth">
                        On team of dedicated freshmen officers, met weekly and planned events to
                        bring the Freshmen class closer together. Plan two banquets, two school-wide parties, and many Freshmen chapels.
                        Manage SGA website as well.
                    </p>
                    <p><span class="noPrint">(<a id="sgaRefLink" class="noPrint nopic" href="#sgaBookmark"><strong>Jump to Recommendation</strong></a>)</span></p>
                    <ul>
                        <li>Attend weekly SGA meetings as well as separate freshmen officer meetings</li>
                        <li>Maintain SGA website for two years</li>
                    </ul>
                </div>
            </div>

            <a class="direct" name="rec"><h3>Recommendations</h3></a>
            <a class="noPrint nopic" name="tands"></a>
            <div class="entry">
                <div class="years">June 16, 2011</div>
                <div class="entrySection">
                    <div class="linked tandsBookmark entryName">Chris Engstrom, Former Colleague, T&S Web Design</div>
                    <?= rec('tands-chris'); ?>
                </div>
            </div>
            <a class="nopic noPrint" name="psiBookmark"></a>
            <div class="entry">
                <div class="years">February 26, 2011</div>
                <div class="entrySection">
                    <div class="psiBookmark entryName linked">Brock Robertson, Former President, Psi Epsilon</div>
                    <?= rec('brock'); ?>
                </div>
            </div>
            <a class="nopic noPrint" name="sgaBookmark"></a>
            <div class="entry">
                <div class="years">February 24, 2011</div>
                <div class="entrySection">
                    <div class="sgaBookmark entryName linked">Andrew Hicks, Former Junior President, OC Student Government Association</div>
                    <?= rec('hicks'); ?>
                </div>
            </div>
            <a class="nopic noPrint" name="northBookmark"></a>
            <div class="entry">
                <div class="years">February 22, 2011</div>
                <div class="entrySection">
                    <div id="northBookmark" class="linked northBookmark entryName">Berlin Fang, Associate Director, North Institute</div>
                    <?= rec('north'); ?>
                </div>
            </div>           

            <div class="entry">
                <div class="years">August 21, 2010</div>
                <div class="entrySection">
                    <div class="linked tandsBookmark entryName">Tim Priebe, Owner, T&S Web Design</div>
                    <?= rec('tands-tim'); ?>
                </div>
            </div>
            <div class="entry">
                <a class="direct" name="ref"><h3>References</h3></a>
                <p>Please contact me for a list of references. <span class="noPrint">(<a href="mailto:adam@ambooth.com?Subject=References Request">Email</a>)</span></p>

            </div>
        </div>
    </body>
</html>
